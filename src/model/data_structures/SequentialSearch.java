package model.data_structures;

public class SequentialSearch<Key,Value> {

	private Node first;
	
	private int n;
	
	private class Node
	{  // linked-list node
	// first node in the linked list
	Key key;
	Value val;
	Node next;
	public Node(Key key, Value val, Node next)
	{
	   this.key  = key;
	   this.val  = val;
	   this.next = next;
	} 
	}
	     public Value get(Key key)
	     {  // Search for key, return associated value.
	        for (Node x = first; x != null; x = x.next)
	           if (key.equals(x.key))
	              return x.val;    // search hit
	        return null;           // search miss
	     }
	     public void put(Key key, Value val)
	     {  // Search for key. Update value if found; grow table if new.
	        for (Node x = first; x != null; x = x.next)
	           if (key.equals(x.key))
	           {  x.val = val; return;  }      // Search hit: update val.
	        first = new Node(key, val, first); // Search miss: add new node.
	     }
	     
	     public int size()
	     {
	    	 return n;
	     }
	     
	     public boolean isEmpty()
	     {
	    	 return n == 0;
	     }
	     
	     public boolean containsKey(Key key)
	     {
	    	 return get(key) != null;
	     }
	     
	     public void delete(Key key) {
	         first = delete(first, key);
	     }
	     
	     private Node delete(Node x, Key key) {
	         if (x == null) return null;
	         if (key.equals(x.key)) {
	             n--;
	             return x.next;
	         }
	         x.next = delete(x.next, key);
	         return x;
	     }
	     
	     public Iterable<Key> keys()  {
	         Queue<Key> queue = new Queue<Key>();
	         for (Node x = first; x != null; x = x.next)
	             queue.put(x.key);
	         return queue;
	     }
}
