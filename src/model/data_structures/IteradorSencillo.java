package model.data_structures;

import java.util.Iterator;

/**
 * Clase que permite iterar sobre una lista secinllamente encadenada
 * @author jh.lake
 * @param <T>
 */
public class IteradorSencillo<T> implements Iterator<T>
{
    /**
     * El nodo donde se encuentra el iterado
     */
    private Nodo<T> actual;
    
    /**
     * Crea un nuevo iterador iniciando en el nodo indicado
     * @param nActual el nodo donde se desea que inicie el iterador
     */
    public IteradorSencillo(Nodo<T> nActual)
    {
        actual = nActual;
    }

    /**
     * Indica si a�n hay elementos por recorrer
     * @return true en caso de que a�n haya elemetos o false en caso contrario
     */
    public boolean hasNext( )
    {
        return actual != null;
    }

    /**
     * Devuelve el siguiente elemento a recorrer
     * post: se actualizado actual al siguiente del actual
     * @return objeto en actual
     */
    public T next( )
    {
        T valor = actual.darObjeto();
        actual = actual.darSiguiente( );
        return valor;
    }

	@Override
	public void remove() {
		// TODO Auto-generated method stub
		
	}


}
