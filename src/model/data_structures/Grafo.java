package model.data_structures;

import java.io.LineNumberReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author cmcast Clase del grafo
 * @param <K> 
 * @param <V>
 */
public class Grafo<K, V> {

	/**
	 * n�mero de arcos en el grafo.
	 */
	private int numeroArcos;

	/**
	 * n�mero de nodos en el grafo.
	 */
	private int numeroNodos;
	

	/**
	 * Hash table que guarda los vertices del grafo.
	 */
	private LinearProbingHashST<K, Vertice<K, V>> vertices;
	
	private SeparateHash<K, Arco<K>> arcos;

	
	
	/**
	 * Caonstructor de la clase.
	 */
	public Grafo() {
		vertices = new LinearProbingHashST<>();
		arcos =  new SeparateHash<>();
		numeroArcos = 0;
		numeroNodos = 0;
	}

	/**
	 * a�ade un vertice al grafo.
	 * 
	 * @param id
	 *            identificador del vertice.
	 * @param obj
	 *            objeto contenido en el vertice.
	 */
	public void agregarVertice(K id, V obj) {
		vertices.put(id, new Vertice<K, V>(id, obj));
		numeroNodos++;
	}

	/**
	 * elimina un vertice del grafo.
	 * @param id
	 * identificador del vertice que se va a eliminar.
	 */
	public void eliminarVertice(K id)
	{
		vertices.delete(id);
		Iterable<K> llaves = vertices.keys();

		for (K key : llaves) {
			Vertice vert = vertices.get(key);
			vert.eliminarArco(id);
		}
		numeroNodos--;
	}

	public int getContNum() {
		return numeroNodos;
	}

	/**
	 * Busca el vertice del identificador dado.
	 * 
	 * @param id
	 *            identificador del vertice buscado.
	 * @return vertice buscado.
	 */
	public Vertice<K, V> buscarVertice(K id) {
		return vertices.get(id);
	}

	/**
	 * A�ade arco a la lista de adyacencia.
	 * 
	 * @param orig
	 *            vertice origen del arco.
	 * @param dest
	 *            vertice destino del arco.
	 * @param peso
	 *            peso del arco.
	 */
	public void agregarArco(K orig, K dest, double peso) {
		Vertice<K, V> vert = vertices.get(orig);
		Vertice<K, V> vert2 = vertices.get(dest);
		vert.agregarArco(dest, peso);
		vert2.agregarArco(orig, peso);
		Arco<K> arc = new Arco<K>(orig, dest, peso);
		arcos.put(orig, arc);
		numeroArcos++;
	}
	

	/**
	 * Elimina un arco de la lista de adyacencia del vector origen.
	 * 
	 * @param orig
	 * vector origen del arco.
	 * @param dest
	 *            vector destino del arco.
	 */
	public void eliminarArco(K orig, K dest) {
		Vertice<K, V> vert = vertices.get(orig);
		vert.eliminarArco(dest);
		numeroArcos--;
	}

	public int getContArco() {
		return numeroArcos;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		String dev = "";
		Iterable<K> hashi = vertices.keys();
		for (K key : hashi) {
			dev += key + "|";
			LinearProbingHashST<K, Arco<K>> arquitos = vertices.get(key).darArcos();
			for (K arco : arquitos.keys()) {
				dev += arco + ",";
			}
			dev += "\n";
		}
		return dev;
	}

	
	
	public void demarcar()
	{
		Iterable<K> q = vertices.keys();
		Iterator<K> i = q.iterator();
		while(i.hasNext())
		{
			Vertice<K,V> v = vertices.get(i.next());
			v.unmark();
		}
	}
	
	public void marcar()
	{
		Iterable<K> q = vertices.keys();
		Iterator<K> i = q.iterator();
		while(i.hasNext())
		{
			Vertice<K,V> v = vertices.get(i.next());
			v.mark();
		}
	}
	
	public List<Vertice<K,V>> darcomoLista()
	{
		Iterable<K> tem = vertices.keys();
		Iterator<K> i = tem.iterator();
		ArrayList<Vertice<K,V>> resp= new ArrayList<Vertice<K,V>>(); 
		while(i.hasNext())
		{
			K key = i.next();
			resp.add(vertices.get(key));
		}
		return resp;
	}
	
	public Arco<K> buscarArco(K pID)
	{
		return arcos.get(pID);
	}
	
	public LinearProbingHashST<K, Vertice<K, V>> darVertices()
	{
		return vertices;
	}
	
	public SeparateHash<K,Arco<K>> darArcos() {
		return arcos;
	}
	

		// throw an IllegalArgumentException unless {@code 0 <= v < V}
    private void validateVertex(int v) {
        if (v < 0 || v >= numeroNodos)
            throw new IllegalArgumentException("vertex " + v + " is not between 0 and " + (numeroNodos-1));
    }
}
