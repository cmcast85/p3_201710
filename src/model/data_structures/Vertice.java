package model.data_structures;

import java.util.Iterator;

/**
 * @author mariadelrosario
 * Clase vertice
 * @param <K>
 * @param <V>
 */
public class Vertice <K, V>
{
	/**
	 * identificador del vertice.
	 */
	protected K id;
	/**
	 * Objeto contenido en el vertice.
	 */
	protected V obj;
	/**
	 * Hash table que almacena la lista de adyacencia de los vertices.
	 */
	private LinearProbingHashST<K, Arco<K>> adj;
	
	/**
	 * Atributo que representa si está o no marcado.
	 */
	private boolean marked;
	
	/**
	 * Constructor del vertice.
	 * @param id identificador del vertice.
	 * @param obj objeto del vertice.
	 */
	public  Vertice (K id, V obj) 
	{
		this.id = id;
		this.obj = obj;
		adj = new LinearProbingHashST<>();
		marked = false;
	}
	
	/**
	 * da el identificador del vertice.
	 * @return id identificador del vertice.
	 */
	public K darId()
	{
		return id;
	}
	
	/**
	 * da el objeto contenido en el vertice.
	 * @return objeto contenido en el vertice.
	 */
	public V darObj ()
	{
		return obj;
	}
	
	/**
	 * Agrega un arco.
	 * @param fin vertice de destino del arco que se va a agregar.
	 * @param costo del arco que se va a agregar.
	 */
	public void agregarArco (K fin, double costo)
	{
		adj.put(fin, new Arco<K>(id, fin, costo));
	}
	
	
	/**
	 *elimina un arco
	 * @param fin vertice de destino del arco a eliminar.
	 */
	public void eliminarArco (K fin)
	{
		Iterable<K> llaves = adj.keys();
		
		for (K key : llaves) 
		{
			Arco<K> arcos = adj.get(key);
			if(arcos.getDestino().equals(fin))
			{
				adj.delete(fin);
				break;
			}
		}
	}
	
	/**
	 * retorna la hash table con la lista de adyacencia.
	 * @return
	 */
	public LinearProbingHashST<K, Arco<K>> darArcos ()
	{
		return adj;
	}
	
	/**
	 * Returns all keys in this symbol table as an {@code Iterable}. To iterate
	 * over all of the keys in the symbol table named {@code st}, use the
	 * foreach notation: {@code for (Key key : st.keys())}.
	 *
	 * @return all keys in this symbol table
	 */
	public Iterator<Arco<K>> keys() {
		Queue<Arco<K>> queue = new Queue<Arco<K>>();
		Iterator<K> values = adj.keys().iterator();
		while(values.hasNext())
		{
			Arco<K> o = adj.get(values.next());
			if (o != null)
				queue.put((Arco<K>) o);
		}
		return queue.iterator();
	}
	
	public boolean isMarked()
	{
		return marked;
	}
	
	public void mark()
	{
		marked = true;
	}
	
	public void unmark()
	{
		marked = false;
	}
	
}

