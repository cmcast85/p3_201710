package model.data_structures;



public class Arco <K>
{
	/**
	 * vertice de origen de un arco
	 */
	private K origen;
	
	/**
	 * vertice de destino de un arco
	 */
	private K destino;
	
	/**
	 * Lista de pesos de un arco
	 */
	private double peso;
	
	/**
	 * @param from vertice origen
	 * @param to vertice destino
	 * @param costo lista de pesos del arco
	 */
	public Arco (K from, K to, double costo)
	{
		origen = from;
		destino = to;
		peso = costo;
	}


	/**
	 *Retorno el vertice origen del arco.
	 * @return vertice origen
	 */
	public K getOrigen() 
	{
		return origen;
	}

	/**
	 * Retorno el vertice destino del arco.
	 * @return vertice destino
	 */
	public K getDestino() 
	{
		return destino;
	}

	/**
	 * Agrega el peso del arco
	 * @param pPeso del arco
	 */
	public void agregarPeso(double pPeso) 
	{
		peso= pPeso;
	}
	
	/**
	 * elimina el peso del arco
	 * @param pPeso del arco
	 */
	public void eliminarPeso( ) 
	{
		peso=0;
	}
	
	public double darPeso()
	{
		return peso;
	}
	
	public K either()
	{
		return origen;
	}
	
	public K other(K k)
	{
		return k.equals(destino) ? origen:destino; 
	}
}
