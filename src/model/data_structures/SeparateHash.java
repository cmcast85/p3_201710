package model.data_structures;

public class SeparateHash<Key,Value> {

	
	public final static int INT_CAPACITY = 32;
	/**
	 * Cantidad de nodos en la tabla
	 */
	private int N;  
	
	/**
	 * Tamaño de la tabla
	 */
    private int M;  
    
    private SequentialSearch<Key, Value>[] st;  
    
    public SeparateHash()
    {  
    	this(INT_CAPACITY);  
    }
    
    public SeparateHash(int Mi)
    {  // Create M linked lists.
       this.M = Mi;
       st = (SequentialSearch<Key, Value>[]) new SequentialSearch[M];
       for (int i = 0; i < M; i++)
          st[i] = new SequentialSearch();
    }
    
    private int hash(Key key)
    {  
    	return (key.hashCode() & 0x7fffffff) % M; 
    }
    
    private void resize(int chains) 
    {
        SeparateHash<Key, Value> temp = new SeparateHash<Key, Value>(chains);
        for (int i = 0; i < M; i++) {
            for (Key key : st[i].keys()) {
                temp.put(key, st[i].get(key));
            }
        }
        this.M  = temp.M;
        this.N  = temp.N;
        this.st = temp.st;
    }
    
    public Value get(Key key)
    {  
    	return (Value) st[hash(key)].get(key);  
    }
    
    public void put(Key key, Value val)
    { 
    	if (key == null) throw new NullPointerException("first argument to put() is null");
        if (val == null) {
            delete(key);
            return;
        }
        
        if (N >= 10*M) resize(2*M);

        
        if (!st[hash(key)].containsKey(key)) N++;
    	st[hash(key)].put(key, val);  
    }
    
    public void delete(Key key)
    {
    	if(key == null) throw new NullPointerException("Argumentes invalid");
    	if(st[hash(key)].containsKey(key)) N--;
    	st[hash(key)].delete(key);
    	
    	if(M > INT_CAPACITY && N < 2*M) resize(M/2);
    }
    
    public Iterable<Key> keys()
    {
    	Queue<Key> queue = new Queue<Key>();
        for (int i = 0; i < M; i++) {
            for (Key key : st[i].keys())
                queue.put(key);
        }
        return queue;
    }
    
    public int size(){
    	return N;
    }
    
    public boolean isEmpty()
    {
    	return N==0;
    }
    
    public boolean containsKey(Key key) {
        return st[hash(key)].containsKey(key);
  }
}
