package model.vo;

import model.data_structures.SeparateHash;

public class VODia {
	
	/**
	 * atributo que contiene los nombres de los teatros con peliculas.
	 */
	private SeparateHash<String, VOPeliculasDia> teatros;
	
	/**
	 * atributo que contiene los ids de las peliculas con sus teatros.
	 */
	
	private SeparateHash<Integer, VOTeatrosDia> peliculas;

	public VODia() {
	}

	public void setTeatros(SeparateHash<String, VOPeliculasDia> teatros) {
		this.teatros = teatros;
	}
	
	public void setPeliculas(SeparateHash<Integer, VOTeatrosDia> peliculas) {
		this.peliculas = peliculas;
	}
	
	
	public SeparateHash<String, VOPeliculasDia> getTeatros() {
		return teatros;
	}
	
	public SeparateHash<Integer, VOTeatrosDia> getPeliculas() {
		return peliculas;
	}
}
