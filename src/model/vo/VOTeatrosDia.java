package model.vo;

import model.data_structures.LinearProbingHashST;
import model.data_structures.ListaDobleEncadenada;

public class VOTeatrosDia {
	
	private LinearProbingHashST<String, ListaDobleEncadenada<Integer>> teatros;
	
	public void setTeatros(LinearProbingHashST<String, ListaDobleEncadenada<Integer>> teatros) {
		this.teatros = teatros;
	}
	
	public LinearProbingHashST<String, ListaDobleEncadenada<Integer>> getTeatros() {
		return teatros;
	}
	
}