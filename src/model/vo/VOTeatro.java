
package model.vo;

/**
 *  
 * @Author: Tomas F. Venegas 
 */
public class VOTeatro {
	
	/**
	 * Atributo que modela el nombre del teatro
	 */
	
	private String nombre;
	
	/**
	 * Atributo que modela la ubicacion del teatro 
	 */
	
	private VOUbicacion ubicacion;
	
	/**
	 * Atributo que referencia la franquicia
	 */
	
	private VOFranquicia franquicia;
	
	
	public VOTeatro() {
	// TODO Auto-generated constructor stub
	}
	
	public String getNombre(){
		return nombre;
	}

	public void setNombre(String nombre){
		this.nombre = nombre;
	}
	
	public VOFranquicia getFranquicia(){
		return franquicia;
	}

	public void setFranquicia(VOFranquicia franquicia){
		this.franquicia = franquicia;
	}
	
	public VOUbicacion getUbicacion(){
		return ubicacion;
	}

	public void setUbicacion(VOUbicacion ubicacion){
		this.ubicacion = ubicacion;
	}
	
}
