package model.vo;

import model.data_structures.LinearProbingHashST;
import model.data_structures.ListaDobleEncadenada;

public class VOPeliculasDia {
	
	private LinearProbingHashST<Integer, ListaDobleEncadenada<Integer>> peliculas;
	
	public void setPeliculas(LinearProbingHashST<Integer, ListaDobleEncadenada<Integer>> peliculas) {
		this.peliculas = peliculas;
	}
	
	public LinearProbingHashST<Integer, ListaDobleEncadenada<Integer>> getPeliculas() {
		return peliculas;
	}
}
