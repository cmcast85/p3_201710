package model.vo;

/**
 *  
 * @Author: Tomas F. Venegas 
 */
public class VOUsuario {
	
	/**
	 * Atributo que modela el id del usuario
	 */

	private long id;
	
	public VOUsuario() {
		// TODO Auto-generated constructor stub
	}
	
	public long getID(){
		return id;
	}
	
	public void set(long id){
		this.id = id;
	}
}
