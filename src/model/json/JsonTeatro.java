package model.json;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class JsonTeatro {

	@SerializedName("Nombre")
	@Expose
	private String nombre;
	@SerializedName("UbicacionGeografica(Lat|Long)")
	@Expose
	private String ubicacion;
	@SerializedName("Franquicia")
	@Expose
	private String franquicia;

	public String getNombre() {
		return nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public String getUbicacionGeograficaLatLong() {
		return ubicacion;
	}
	
	public void setUbicacionGeograficaLatLong(String ubicacionGeograficaLatLong) {
		this.ubicacion = ubicacionGeograficaLatLong;
	}
	
	public String getFranquicia() {
		return franquicia;
	}
	
	public void setFranquicia(String franquicia) {
		this.franquicia = franquicia;
	}
}
