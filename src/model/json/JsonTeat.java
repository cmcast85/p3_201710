package model.json;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class JsonTeat {

	@SerializedName("peliculas")
	@Expose
	private List<JsonPelicula> peliculas = null;
	
	public List<JsonPelicula> getPeliculas() {
		return peliculas;
	}
	
	public void setPeliculas(List<JsonPelicula> peliculas) {
		this.peliculas = peliculas;
	}

}