package model.json;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class JsonFunciones {

	@SerializedName("teatros")
	@Expose
	private String nombre;
	@SerializedName("teatro")
	@Expose
	private JsonTeat teatro;
	
	public String getNombre() {
		return nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public JsonTeat getTeatro() {
		return teatro;
	}
	
	public void setTeatro(JsonTeat teatro) {
		this.teatro = teatro;
	}
}
