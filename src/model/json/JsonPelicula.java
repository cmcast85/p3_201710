package model.json;


import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class JsonPelicula {

	@SerializedName("id")
	@Expose
	private Integer id;
	@SerializedName("funciones")
	@Expose
	private List<JsonFunc> funciones = null;
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public List<JsonFunc> getFunciones() {
		return funciones;
	}
	
	public void setFunciones(List<JsonFunc> funciones) {
		this.funciones = funciones;
	}
}