package model.json;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class JsonDistancia {

	@SerializedName("Teatro 1")
	@Expose
	private String teatro1;
	@SerializedName("Teatro 2")
	@Expose
	private String teatro2;
	@SerializedName("Tiempo (minutos)")
	@Expose
	private Integer tiempoMinutos;
	
	public String getTeatro1() {
		return teatro1;
	}
	
	public void setTeatro1(String teatro1) {
		this.teatro1 = teatro1;
	}
	
	public String getTeatro2() {
		return teatro2;
	}
	
	public void setTeatro2(String teatro2) {
		this.teatro2 = teatro2;
	}
	
	public Integer getTiempoMinutos() {
		return tiempoMinutos;
	}
	
	public void setTiempoMinutos(Integer tiempoMinutos) {
		this.tiempoMinutos = tiempoMinutos;
	}
}