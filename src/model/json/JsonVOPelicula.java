package model.json;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class JsonVOPelicula {

	@SerializedName("movie_id")
	@Expose
	private Integer movieId;
	@SerializedName("title")
	@Expose
	private String title;
	@SerializedName("genres")
	@Expose
	private String genres;
	
	public Integer getMovieId() {
		return movieId;
	}
	
	public void setMovieId(Integer movieId) {
		this.movieId = movieId;
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getGenres() {
		return genres;
	}
	
	public void setGenres(String genres) {
		this.genres = genres;
	}
}