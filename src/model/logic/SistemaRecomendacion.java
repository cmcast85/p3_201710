package model.logic;

import model.data_structures.Arco;
import model.data_structures.EdgeWeightedGraph;
import model.data_structures.LinearProbingHashST;
import model.data_structures.ListaDobleEncadenada;
import model.data_structures.MinPQ;
import model.data_structures.SeparateHash;
import model.json.JsonDistancia;
import model.json.JsonFunc;
import model.json.JsonFunciones;
import model.json.JsonTeatro;
import model.json.JsonVOPelicula;
import model.json.JsonPelicula;
import model.vo.VODia;
import model.vo.VOFranquicia;
import model.vo.VOGeneroPelicula;
import model.vo.VOPelicula;
import model.vo.VOPeliculaPlan;
import model.vo.VOPeliculasDia;
import model.vo.VOTeatro;
import model.vo.VOTeatrosDia;
import model.vo.VOUbicacion;
import model.vo.VOUsuario;

import java.io.BufferedReader;
import java.io.FileReader;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;

import API.ILista;
import API.ISistemaRecomendacion;

public class SistemaRecomendacion implements ISistemaRecomendacion{

	
	
	/**
	 * Lista con los teatros. Las llaves son el nombre de la sala.
	 */
	private SeparateHash<String, VOTeatro> teatros;

	private LinearProbingHashST<String, Integer> teatrosId;
	private LinearProbingHashST<Integer, String> idTeatros;
	
	/**
	 * Lista con las peliculas. Las llaves son el ID.
	 */
	private SeparateHash<Integer, VOPelicula> peliculas;
	
	/**
	 * Lista con la informaci�n de la cartelera. La llave es el d�a.
	 */
	private VODia dia[];
	
	private SeparateHash<Integer, MinPQ<VOPeliculaPlan>> peliculasDia;
	
	private SeparateHash<Integer, SeparateHash<Integer, ListaDobleEncadenada<VOPeliculaPlan>>> peliculasDiaHora;
	/**
	 * Grafo de las peliculas. La llave es el nombre de un teatro.
	 */
	//private Grafo<String, VOTeatro> grafo;
	
	private EdgeWeightedGraph<Integer> grafo;
	
	public SistemaRecomendacion() {
		teatros = new SeparateHash<>();
		peliculas = new SeparateHash<>();
		dia = new VODia[5];
		peliculasDia = new SeparateHash<>();

		teatrosId = new LinearProbingHashST<>();
		idTeatros = new LinearProbingHashST<>();
	}
	
	/**
	 * Carga a un SeparateHash los teatros con sus franquicias y ubicaciones.
	 * 
	 * @param archivo : ruta del archivo json.
	 */
	public boolean cargarTeatros(String archivo){
		try {
			JsonReader reader = new JsonReader(new FileReader(archivo));
			Gson gson = new Gson();
			JsonTeatro[] salas = gson.fromJson(reader, JsonTeatro[].class);
			for(JsonTeatro sala : salas) {
				VOTeatro nuevo = new VOTeatro();
				
				nuevo.setNombre(sala.getNombre());
				
				nuevo.setFranquicia(new VOFranquicia(sala.getFranquicia()));
				String ubicacion[] = sala.getUbicacionGeograficaLatLong().split("\\|");
				double lat = Double.parseDouble(ubicacion[0]);
				double lon = Double.parseDouble(ubicacion[1]);
				nuevo.setUbicacion(new VOUbicacion(lat,lon));
				
				teatros.put(nuevo.getNombre(), nuevo);
				//System.out.println(nuevo.getNombre());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	/**
	 * Carga de una carpeta los 5 archivos de d�as que hay para el festival a un vector de VODia.
	 * 
	 * @param carpetaArchivos
	 */
	public boolean cargarCartelera(String carpetaArchivos) {
		Gson gson = new Gson();
		
		//Carga de peliculas
		try {
			String nombreArchivo = carpetaArchivos + "movies_filtered.json";
			JsonReader reader = new JsonReader(new FileReader(nombreArchivo));
			JsonVOPelicula[] peliculas = gson.fromJson(reader, JsonVOPelicula[].class);
			for (JsonVOPelicula pelicula : peliculas) {
				int id = pelicula.getMovieId();
				String nombre = pelicula.getTitle();
				String generos[] = pelicula.getGenres().split("\\|");
				this.peliculas.put(id, new VOPelicula(id, nombre, generos));
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		
		//Carga cartelera
		for (int i = 0; i < 5; i++) {
			String rutaArchivo = carpetaArchivos.concat("/programacion/dia" + (i+1) + ".json");
			
			/*
			 * lista de teatros, contiene lista de peliculas que contiene lista de horarios
			 */
			SeparateHash<String, VOPeliculasDia> teatros = new SeparateHash<>();
			
			/*
			 * lista de peliculas, contiene lista de teatros que contiene lista de horarios
			 */
			SeparateHash<Integer, VOTeatrosDia> peliculas = new SeparateHash<>();
			
			MinPQ<VOPeliculaPlan> porHoras = new MinPQ<>();
			
			//System.out.println("DIA " + (i+1));
			try {
				JsonReader reader = new JsonReader(new FileReader(rutaArchivo));
				JsonFunciones[] funciones = gson.fromJson(reader, JsonFunciones[].class);
				
				for (JsonFunciones teatro : funciones) {
					
					LinearProbingHashST<Integer, ListaDobleEncadenada<Integer>> horasPeliculas = new LinearProbingHashST<>();
					ListaDobleEncadenada<Integer> fun = new ListaDobleEncadenada<>();
					String nombre = teatro.getNombre();
					//System.out.println(nombre);
					
					for (JsonPelicula pel : teatro.getTeatro().getPeliculas()) {
						fun = new ListaDobleEncadenada<>();
						//System.out.println("\t" + pel.getId());
						for (JsonFunc func : pel.getFunciones()) {
							String hora[] = func.getHora().split(":");
							int hr;
							if(hora[1].endsWith("p.m.") && !hora[0].equals("12")) hr = Integer.parseInt(hora[0]) + 12;
							else hr = Integer.parseInt(hora[0]);
							VOPeliculaPlan nueva = new VOPeliculaPlan();
							nueva.setPelicula(this.peliculas.get(pel.getId()));
							nueva.setTeatro(this.teatros.get(teatro.getNombre()));
							nueva.setHoraInicio(hr);
							nueva.setHoraFin(hr+2);
							nueva.setDia(i+1);
							porHoras.insert(nueva);
							fun.agregarElementoFinal(hr);
							//System.out.println("\t\t" + hr);
						}
						horasPeliculas.put(pel.getId(), fun);
						
						if (peliculas.containsKey(pel.getId())) {
							VOTeatrosDia viejo = peliculas.get(pel.getId());
							viejo.getTeatros().put(teatro.getNombre(), fun);
						} else {
							VOTeatrosDia nuevo = new VOTeatrosDia();
							LinearProbingHashST<String, ListaDobleEncadenada<Integer>> horasTeatros = new LinearProbingHashST<>();
							horasTeatros.put(teatro.getNombre(), fun);
							nuevo.setTeatros(horasTeatros);
							peliculas.put(pel.getId(), nuevo);
						}
					}
					VOPeliculasDia nuevasPeliculas = new VOPeliculasDia();
					nuevasPeliculas.setPeliculas(horasPeliculas);
					teatros.put(nombre, nuevasPeliculas);
				}
				
			} catch (Exception e) {
				e.printStackTrace();
				return false;
			}

			dia[i] = new VODia();
			dia[i].setTeatros(teatros);
			dia[i].setPeliculas(peliculas);
			
			peliculasDia.put(i+1, porHoras);
			
			/*for (Integer id : peliculas.keys()) {
				System.out.println("Pelicula: " + id);
				for (String teat : peliculas.get(id).getTeatros().keys()) {
					System.out.println("\t" + teat);
					for(String hora : peliculas.get(id).getTeatros().get(teat)) {
						System.out.println("\t\t" + hora);
					}
				}
			}*/
		}
		peliculasDiaHora = new SeparateHash<>();
		for(Integer dia : peliculasDia.keys()) {
			MinPQ<VOPeliculaPlan> pels = peliculasDia.get(dia);
			int hora = 0;
			SeparateHash<Integer, ListaDobleEncadenada<VOPeliculaPlan>> peliculasPlan = new SeparateHash<>();
			ListaDobleEncadenada<VOPeliculaPlan> peliculasHora = new ListaDobleEncadenada<>();
			for (VOPeliculaPlan pelicula : pels) {
				if(pelicula.getHoraInicio() == hora) {
					peliculasHora.agregarElementoFinal(pelicula);
				}
				else if (hora == 0) {
					hora = pelicula.getHoraInicio();
					peliculasHora.agregarElementoFinal(pelicula);
				}
				else if(pelicula.getHoraInicio() > hora) {
					peliculasPlan.put(hora, peliculasHora);
					hora = pelicula.getHoraInicio();
					peliculasDiaHora.put(dia, peliculasPlan);
					peliculasHora = new ListaDobleEncadenada<>(pelicula);
				}
			}
		}
		return true;
	}
	
	public boolean cargarRed(String archivo) {
		int tam = teatros.size();
		grafo = new EdgeWeightedGraph<Integer>(tam);
		
		int i = 0;
		for (String teatro : teatros.keys()) {
			teatrosId.put(teatro, i);
			idTeatros.put(i, teatro);
			i++;
		}
		
		Gson gson = new Gson();
		try {
			JsonReader reader = new JsonReader(new FileReader(archivo));
			JsonDistancia[] arcos = gson.fromJson(reader, JsonDistancia[].class);
			for (JsonDistancia arco : arcos) {
				int teatro1 = teatrosId.get(arco.getTeatro1());
				int teatro2 = teatrosId.get(arco.getTeatro2());
				Arco<Integer> nuevo = new Arco<>(teatro1, teatro2, arco.getTiempoMinutos());
				grafo.addEdge(nuevo);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	@Override
	public ISistemaRecomendacion crearSR() {
		return this;
	}

	@Override
	public int sizeMovies() {
		return peliculas.size();
	}

	@Override
	public int sizeTeatros() {
		return teatros.size();
	}

	@Override
	public ILista<VOPeliculaPlan> PlanPeliculas(VOUsuario usuario, int fecha) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ILista<VOPeliculaPlan> PlanPorGenero(VOGeneroPelicula genero, VOUsuario usuario) {
		ListaDobleEncadenada<VOPelicula> retorno = new ListaDobleEncadenada<>();
		
		int day = 0;
		while(day<5){
			
			peliculasDiaHora.get(day);
			
		}
		return null;
	}
	
	@Override
	public ListaDobleEncadenada<VOPeliculaPlan> PlanPorFranquicia(VOFranquicia franquicia, int fecha, String franja) {
		franja = franja.toLowerCase();
		VODia dia = this.dia[fecha-1];
		boolean manana = false;
		boolean tarde = false;
		boolean noche = false;
		
		ListaDobleEncadenada<VOPeliculaPlan> retornar = new ListaDobleEncadenada<>();
		
		if (franja.contains("manana")) {
			manana = true;
			MinPQ<VOPeliculaPlan> horas = peliculasDia.get(fecha);
			
		}
		
		if (franja.contains("tarde")) {
			tarde = true;
		}
		
		if (franja.contains("noche")) {
			noche = true;
		}
		
		return retornar;
	}

	@Override
	public ILista<VOPeliculaPlan> PlanPorGeneroYDesplazamiento(VOGeneroPelicula genero, int fecha) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ILista<VOPeliculaPlan> PlanPorGeneroDesplazamientoYFranquicia(VOGeneroPelicula genero, int fecha,
			VOFranquicia franquicia) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ListaDobleEncadenada<Arco<VOTeatro>> generarMapa() {
		ListaDobleEncadenada<Arco<VOTeatro>> retornar = new ListaDobleEncadenada<>();
		Prim prim = new Prim(grafo);
		//int j = 1;
		//int sum = 0;
		for (Arco<Integer> i : prim.Edges()) {
			VOTeatro teatro1 = teatros.get(idTeatros.get(i.getOrigen()));
			VOTeatro teatro2 = teatros.get(idTeatros.get(i.getDestino()));
			//System.out.println(j + ". \t" + teatro1.getNombre() + " - " + teatro2.getNombre());
			//sum+=i.darPeso();
			//System.out.println("   " + (int)i.darPeso() + " mins");
			//System.out.println("\t\t\t\t\t\t\t\t\t" + sum + " mins");
			Arco<VOTeatro> nuevo = new Arco<>(teatro1, teatro2, i.darPeso());
			retornar.agregarElementoFinal(nuevo);
			//j++;
		}
		System.out.println("Tiempo total desplazamiento : " + (int)prim.weight() + " mins");
		return retornar;
	}
	/**
	 * ESTE METODO NO TIENE SENTIDO
	 * TODOS LOS NODOS ESTAN CONECTADOS CON TODOS LOS DEMAS
	 * TERMINARIAMOS TENIENDO UNA LISTA DE 44 + 44*43 + 43*42 + 42*41 .... = 475409 CAMINOS
	 */
	@Override
	public ListaDobleEncadenada<ListaDobleEncadenada<VOTeatro>> rutasPosible(VOTeatro origen, int n) {
		ListaDobleEncadenada<ListaDobleEncadenada<VOTeatro>> retornar = new ListaDobleEncadenada<>();
		ListaDobleEncadenada<VOTeatro> camino = new ListaDobleEncadenada();
		//int inicio = teatrosId.get(origen.getNombre());
		//boolean [] marcado = new boolean[grafo.V()];
		int resultado = 0;
		for (int i = 44; i > 0; i--) {
			for (int j = i - 1; j > 0; j--) resultado += j*i;
		} 
		System.out.println("numero de caminos posibles: " + (resultado+44));
		//marcado[inicio] = true;
		
		return retornar;
	}
}
