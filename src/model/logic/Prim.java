package model.logic;

import model.data_structures.IndexMinPQ;
import model.data_structures.Queue;
import model.data_structures.UF;
import model.data_structures.Arco;
import model.data_structures.EdgeWeightedGraph;

/******************************************************************************
 *  Compilation:  javac PrimMST.java
 *  Execution:    java PrimMST filename.txt
 *  Dependencies: EdgeWeightedDigraph<Integer>.java Arco<Integer>.java Queue.java
 *                IndexMinPQ.java UF.java In.java StdOut.java
 *  Data files:   http://algs4.cs.princeton.edu/43mst/tinyEWG.txt
 *                http://algs4.cs.princeton.edu/43mst/mediumEWG.txt
 *                http://algs4.cs.princeton.edu/43mst/largeEWG.txt
 *
 *  Compute a minimum spanning forest using Prim's algorithm.
 *
 *  %  java PrimMST tinyEWG.txt 
 *  1-7 0.19000
 *  0-2 0.26000
 *  2-3 0.17000
 *  4-5 0.35000
 *  5-7 0.28000
 *  6-2 0.40000
 *  0-7 0.16000
 *  1.81000
 *
 *  % java PrimMST mediumEWG.txt
 *  1-72   0.06506
 *  2-86   0.05980
 *  3-67   0.09725
 *  4-55   0.06425
 *  5-102  0.03834
 *  6-129  0.05363
 *  7-157  0.00516
 *  ...
 *  10.46351
 *
 *  % java PrimMST largeEWG.txt
 *  ...
 *  647.66307
 *
 ******************************************************************************/

/**
 *  The {@code PrimMST} class represents a data type for computing a
 *  <em>minimum spanning tree</em> in an Arco<Integer>-weighted graph.
 *  The Arco<Integer> weights can be positive, zero, or negative and need not
 *  be distinct. If the graph is not connected, it computes a <em>minimum
 *  spanning forest</em>, which is the union of minimum spanning trees
 *  in each connected component. The {@code weight()} method returns the 
 *  weight of a minimum spanning tree and the {@code Arco<Integer>s()} method
 *  returns its Arco<Integer>s.
 *  <p>
 *  This implementation uses <em>Prim's algorithm</em> with an indexed
 *  binary heap.
 *  The constructor takes time proportional to <em>E</em> log <em>V</em>
 *  and extra space (not including the graph) proportional to <em>V</em>,
 *  where <em>V</em> is the number of vertices and <em>E</em> is the number of Arco<Integer>s.
 *  Afterwards, the {@code weight()} method takes constant time
 *  and the {@code Arco<Integer>s()} method takes time proportional to <em>V</em>.
 *  <p>
 *  For additional documentation,
 *  see <a href="http://algs4.cs.princeton.edu/43mst">Section 4.3</a> of
 *  <i>Algorithms, 4th Edition</i> by Robert SArco<Integer>wick and Kevin Wayne.
 *  For alternate implementations, see {@link LazyPrimMST}, {@link KruskalMST},
 *  and {@link BoruvkaMST}.
 *
 *  @author Robert SArco<Integer>wick
 *  @author Kevin Wayne
 */
public class Prim {
    private static final double FLOATING_POINT_EPSILON = 1E-12;

    private Arco<Integer>[] EdgeTo;        // EdgeTo[v] = shortest Arco<Integer> from tree vertex to non-tree vertex
    private double[] distTo;      // distTo[v] = weight of shortest such Arco<Integer>
    private boolean[] marked;     // marked[v] = true if v on tree, false otherwise
    private IndexMinPQ<Double> pq;

    /**
     * Compute a minimum spanning tree (or forest) of an Arco<Integer>-weighted graph.
     * @param G the Arco<Integer>-weighted graph
     */
    public Prim(EdgeWeightedGraph<Integer> G) {
        EdgeTo = new Arco[G.V()];
        distTo = new double[G.V()];
        marked = new boolean[G.V()];
        pq = new IndexMinPQ<Double>(G.V());
        for (int v = 0; v < G.V(); v++)
            distTo[v] = Double.POSITIVE_INFINITY;

        for (int v = 0; v < G.V(); v++)      // run from each vertex to find
            if (!marked[v]) prim(G, v);      // minimum spanning forest

        // check optimality conditions
        assert check(G);
    }

    // run Prim's algorithm in graph G, starting from vertex s
    private void prim(EdgeWeightedGraph<Integer> G, int s) {
        distTo[s] = 0.0;
        pq.insert(s, distTo[s]);
        while (!pq.isEmpty()) {
            int v = pq.delMin();
            scan(G, v);
        }
    }

    // scan vertex v
    private void scan(EdgeWeightedGraph<Integer> G, int v) {
        marked[v] = true;
        for (Arco<Integer> e : G.adj(v)) {
            int w = e.other(v);
            if (marked[w]) continue;         // v-w is obsolete edge
            if (e.darPeso() < distTo[w]) {
                distTo[w] = e.darPeso();
                EdgeTo[w] = e;
                if (pq.contains(w)) pq.decreaseKey(w, distTo[w]);
                else                pq.insert(w, distTo[w]);
            }
        }
    }

    /**
     * Returns the edges in a minimum spanning tree (or forest).
     * @return the edges in a minimum spanning tree (or forest) as
     *    an iterable of edges
     */
    public Iterable<Arco<Integer>> Edges() {
        Queue<Arco<Integer>> mst = new Queue<Arco<Integer>>();
        for (int v = 0; v < EdgeTo.length; v++) {
            Arco<Integer> e = (model.data_structures.Arco<Integer>) EdgeTo[v];
            if (e != null) {
                mst.put(e);
            }
        }
        return mst;
    }

    /**
     * Returns the sum of the edges weights in a minimum spanning tree (or forest).
     * @return the sum of the edges weights in a minimum spanning tree (or forest)
     */
    public double weight() {
        double weight = 0.0;
        for (Arco<Integer> e : Edges())
            weight += e.darPeso();
        return weight;
    }


    // check optimality conditions (takes time proportional to E V lg* V)
    private boolean check(EdgeWeightedGraph<Integer> G) {

        // check weight
        double totalWeight = 0.0;
        for (Arco<Integer> e : Edges()) {
            totalWeight += e.darPeso();
        }
        if (Math.abs(totalWeight - weight()) > FLOATING_POINT_EPSILON) {
            System.err.printf("Weight of edges does not equal weight(): %f vs. %f\n", totalWeight, weight());
            return false;
        }

        // check that it is acyclic
        UF uf = new UF(G.V());
        for (Arco<Integer> e : Edges()) {
            int v = e.either(), w = e.other(v);
            if (uf.connected(v, w)) {
                System.err.println("Not a forest");
                return false;
            }
            uf.union(v, w);
        }

        // check that it is a spanning forest
        for (Arco<Integer> e : G.edges()) {
            int v = e.either(), w = e.other(v);
            if (!uf.connected(v, w)) {
                System.err.println("Not a spanning forest");
                return false;
            }
        }

        // check that it is a minimal spanning forest (cut optimality conditions)
        for (Arco<Integer> e : Edges()) {

            // all Edges in MST except e
            uf = new UF(G.V());
            for (Arco<Integer> f : Edges()) {
                int x = f.either(), y = f.other(x);
                if (f != e) uf.union(x, y);
            }

            // check that e is min weight edge in crossing cut
            for (Arco<Integer> f : G.edges()) {
                int x = f.either(), y = f.other(x);
                if (!uf.connected(x, y)) {
                    if (f.darPeso() < e.darPeso()) {
                        System.err.println("Edge " + f + " violates cut optimality conditions");
                        return false;
                    }
                }
            }

        }

        return true;
    }
 }