package API;



public interface ILista<T> extends Iterable<T>{
	
	
	
	public void agregarElementoFinal(T elem);
	
	public T darElemento(int pos);
	
	public boolean eliminarElemento(int pos);
	
	public int darNumeroElementos();

	public boolean avanzarSiguientePosicion();

	public boolean retrocederPosicionAnterior();

	public T darElementoPosicionActual();

	boolean contains(T gen);

	boolean isEmpty();

	public void agregarElementoFinalNorepetido(T gen);

	public void restablecerActual();

	

}
